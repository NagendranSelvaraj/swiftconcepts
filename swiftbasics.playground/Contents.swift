import UIKit

var str = "Hello, playground"

// Optionals

var myAge: Int?
myAge = 23
print(myAge!)

print("myAge is \(myAge ?? 0)")
// safe method
if let age = myAge {
    print(age)
}

var name: String? = "navy"

print("myName is \(name ?? "nagu")")

if let myName = name {
    print(myName)
}

// Constant and Variable
// let -> Constant
// var -> Variable

/*
let value = 55
value = 60
print(value)
*/

var firstName: String!
firstName = "nagendran"
print(firstName!)

// Operators
// + - * /
var a = 10
var b = 20
print("add = \(a + b) sub = \(a - b) mul = \(a * b) div = \(a / b)")

// Tuples

var clg = ("MCA",199,"A")
let (dept,rollNo,section) = clg
print("Dept : \(dept)")
print("RollNo : \(rollNo)")
print("Section : \(section)")

let http404Error = (404, "Not Found")

let (statusCode, statusMessage) = http404Error
print("statusCode :\(statusCode)")
print("statusMessage :\(statusMessage)")

let (justTheStatusCode, _) = http404Error
print("statusCode :\(justTheStatusCode)")



// print
print("----------------------------------print----------------------------------")

// Datatypes
var num: Int = 10
var sum: Float = 2.2
var c: Double = 14.2
var d: Bool = true
var e: String = "Nagu"
var f: Character = "A"

print(num == 10)
print(sum)
print(c)
print(d)
print(e)
print(f)

//if condition
print("------------------------if loop---------------------------")
let number = 10
if number > 0 {
    print("positive")
} else {
    print("negative")
}

let value = 20
if value > 25 {
    print("true")
} else if value <= 20 {
    print("false")
} else {
    print("error")
}

// for loop
print("---------------------------for loop-----------------------------")
let numbers = ["one","two","three"]
for number in numbers {
    print(number)
}

let person = ["nagu": 23,"navy": 22,"pavi": 28,"raghu": 20]
for (name,age) in (person) {
    print("Name :\(name) and age :\(age)")
}

let count = 5
for counter in 0..<count {
    print(counter)
}

let time = 500
let interval = 100
for work in stride(from: 0, to: time, by: interval) {
    print(work)
}

//while loop
print("---------------------whileloop------------------")
var x = 5
var y = 10
while x < y {
    x = x+1
    print("\(x) is not equal \(y)")
}

//Function
print("--------------------Function-------------------")
func firstUser() { // without parameter
    print("welcome")
}
firstUser()

func lastUser(name: String) { // with parameter
    print("Myname is \(name)")
}
lastUser(name: "navynagu")
let personName = "raju"
lastUser(name: personName)
print("--------------------------------------------------")

func proUser(person: String) -> String {
    return "Universe, " + person + "!"
}
print(proUser(person: "aravind"))

func newUser(person: String) -> String {
    return "Universe, " + person + "!"
}
print(newUser(person: "nagendran"))

func user(person: String, valid: Bool) ->String {
    if valid {
        return newUser(person: person)
    } else {
        return proUser(person: person)
    }
}
print(user(person: "bala", valid: true))

// enum
print("----------------enum-----------------")
enum Direction: String {
    case north = "North"
    case south = "South"
    case east = "East"
    case west = "West"
    
    func benefits() {
        switch self {
        case .north:
            print("North Direction")
        case .south:
            print("South Direction")
        case .east:
            print("East Direction")
        case .west:
            print("West Direction")
        }
    }
}
let direction = Direction.west
direction.rawValue
direction.benefits()

// struct
print("---------------struct---------------")
struct Person {
    var name,phoneNumber: String
    var id,age: Int
    
    func unique() {
        print("\(name)")
        print("\(phoneNumber)")
        print("\(id)")
        print("\(age)")
        
    }
}
let student = Person(name: "nagu", phoneNumber: "9597040531", id: 118, age: 23)
student.unique()
let student1 = Person(name: "ramkumar", phoneNumber: "8667893341", id: 119, age: 23)
student1.unique()
print("---------------------------------------------------------------")

struct Ticket {
    var id: String
    var seatNo: Int
    var cost: Int
    var time: String
    
    func show() {
        print("\(id)")
        print("\(seatNo)")
        print("\(cost)")
        print("\(time)")
    }
}
let mandela = Ticket(id: "10", seatNo: 77, cost: 100, time: "10.30PM")
mandela.show()

struct Fahrenheit {
    let temp: Double
    init() {
        temp = 44.0
    }
}
let temparature = Fahrenheit()
print("temparature is: \(temparature.temp)")

//class
print("---------------class-------------")

class Views {
    var name: String = "navy"
    var age: Int = 25
}
class view: Views {
    func part() {
        print("\(name)")
        print("\(age)")
    }
}
let parts = view()
parts.part()

//Cloures
print("-------------------Cloures-----------------")
var customerInLine = ["nagu","raju","ravi","aravind","ram"]
print(customerInLine.count)

let customerProfile = {customerInLine.remove(at: 4)}
print(customerInLine.count)

print("remove name: \(customerProfile())!")
print(customerInLine.count)


func close(cloure: () -> Void) {
    cloure()
}
class SomeClass {
    var x = 10
    func doSomething() {
        close { self.x = 10 }
        close { x = 20 }
    }
}
let instance = SomeClass()
instance.doSomething()
print(instance.x)

print("----------------------------------------------------------------------------------------------------")

let possibleString: String? = "An optional string."
let forcedString: String = possibleString!

let assumedString: String! = "An implicity unwrapped optional string."
let implicitString:String = assumedString

if assumedString != nil {
    print(assumedString!)
}
if let definitionString = possibleString {
    print(definitionString)
}
// Error handling
print("------------------------------Errorhandling--------------------------")
func canThrowAnError() throws {
    
}
do {
    try canThrowAnError()
    // no error was thrown
} catch {
    // an error was throw
}

print("-------------------------------------------------------------------")

enum LoginValidationError: Error {
    case emptyEmail
    case invalidEmail
    case emptyPassword
    case invalidPassword
    
    var ErrorMessage: String {
        switch self {
        case .emptyEmail:
            return "Enter the email"
        case .invalidEmail:
            return "Enter vaild email"
        case .emptyPassword:
            return "Enter the password"
        case .invalidPassword:
            return "Enter valid password"
        }
    }
}
class LoginHelper {
    func login(email: String?,password: String?) throws {
        guard let email = email else {
            throw LoginValidationError.emptyEmail
        }
        guard email.contains("@") else {
            throw LoginValidationError.invalidEmail
        }
        guard let password = password else {
            throw LoginValidationError.emptyPassword
        }
        guard password.count > 6 else {
            throw LoginValidationError.invalidPassword
        }
        print("login successfully")
    }
}
let user = LoginHelper()
do {
    try user.login(email: "Nagu@gmail.com", password: "naguna1")
} catch let error as LoginValidationError {
    print(error.ErrorMessage)
}
//Generics
print("----------------------Generics-----------------------")
func configure<T: Numeric>(num: T,num1: T) {
    let total = num + num1
    print(total)
}
configure(num: 10, num1: 20)

struct Queue<T> {
    var elements: [T] = []
    
    mutating func insert(element: T) {
        elements.append(element)
    }
    mutating func remove() -> T? {
        guard let firstElement = elements.first else {
            return nil
        }
        elements.remove(at: 0)
        return firstElement
    }
}
var q = Queue<Int>()

q.insert(element: 10)
q.insert(element: 20)
q.remove()
q.remove()
q.remove()
print("-------------------------------------------------------------------------------------------------------")

class DataImporter {
    var filename = "data.text"
}
class DataManger {
    lazy var importer = DataImporter()
    var data = [String]()
}
let manager = DataManger()
manager.data.append("Some data")
manager.data.append("Some more data")
print(manager.importer.filename)

// ControlFlow
print("------------------ControlFlow-----------------")
   // continue
   //break
   //throw
   //return
   //fallthrough

// continue
for i in 1...10 {
    if i % 3 == 0 {
        continue
       // print("Mutiple of 3: \(i)")
    }
    print("no: \(i)")
}
//break
for i in 1...10 {
    if i % 3 == 0 {
        break
    }
    print("no :\(i)")
}
//fallthrough
let numberValue = 1

switch numberValue {
case 1:
    print("case 1")
    fallthrough
case 2:
    print("case 2")
case 3:
    print("case 3")
default:
    print("defualt")
}
// return -> depend upon the functionality
// throw -> error throw

print("-----------------------protocols----------------------")

protocol MiliterDog {
    func findBomb()
    func helpSolider()
}

struct Dog {
    func bark() {
        print("barking")
    }
    func eat() {
        print("eating")
    }
}
extension Dog: MiliterDog {
    func findBomb() {
        print("findingBomb")
    }
    func helpSolider() {
        print("helping solider")
    }
}
let dog = Dog()
dog.bark()
dog.eat()
dog.findBomb()
dog.helpSolider()

protocol Togglable {
    mutating func toggle()
}
enum OnOffSwitch: Togglable {
    case on,off
    mutating func toggle() {
        switch self {
        case .on:
            self = .on
        case .off:
            self = .off
        }
    }
}
var light  = OnOffSwitch.off
light.toggle()

// Array
print("----------------------------Array----------------------------")

var array = ["dog","cat","horse"]
for item in array{
    print(item)
}
print("----------------------------------------------------------------------------------")
var myArray = ["one","two","three"]
for counter in 0...myArray.count - 1 {
    myArray[counter] = "My" + myArray[counter]
    print(myArray[counter])
}

// Empty Array
var emptyInterger = [Int]() //Int
print(emptyInterger)

var emptyString = [String]() //String
print(emptyString)

var emptyArray: [Int] = [1,2,3,4]
print(emptyArray)

var language = ["swift","objective C","java"]
print(language)
print(language[0])
language.append("python")
print(language)

var actualInteger = [10,20,30,40]
print("Before insert \(actualInteger)")
actualInteger.insert(15, at: 1)
print(actualInteger)
actualInteger.remove(at: 0)
print(actualInteger)

var address: [Any] = ["madurai",49]
print(address)

//Dictionaries
print("--------------------dictionary----------------------")
var dict = [Int: String]()
print(dict)

var city: [Int: String] = [1: "madurai",2:"chennai",3: "kovai"]
print(city)

var someDict:[Int:String] = [1:"One", 2:"Two", 3:"Three"]
var someVar = someDict[1]

print( "Value of key = 1 is \(someVar)" )
print( "Value of key = 2 is \(someDict[2])" )
print( "Value of key = 3 is \(someDict[3])" )

var values:[Int:String] = [1:"One", 2:"Two", 3:"Three"]

let dictKeys = [Int](values.keys)
let dictValues = [String](values.values)

print("Print Dictionary Keys")

for (key) in dictKeys {
   print("\(key)")
}
print("Print Dictionary Values")

for (value) in dictValues {
   print("\(value)")
}
